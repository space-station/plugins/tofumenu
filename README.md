# Tofu Menu
(formerly Fedora Menu)


Quick access menu for the GNOME panel with options that help ease the workflow for newcomers and power users alike.

Similar to the Apple Menu found on MacOS.


## Options include:

-About My System (opens a submenu in System Settings that shows info about your computer.)

-System Settings

-Software Center (defaults to `gnome-software`)

-Activities (this menu does replace the Activities button, but this option does leave that functionality easily accessible.)

-Force Quit App (run this and select the app you want to kill. Requires `xkill` )

-Terminal (defaults to `gnome-terminal`)

-Extensions (Quick access to all of your GNOME Extensions. Requires `gnome-extensions-app`)


fork of [Big Sur Menu by fausto](https://extensions.gnome.org/extension/3703/big-sur-menu/)

Compatible with and tested on GNOME 40. Should work on older versions.


## Installation

* Manual : Grab the latest release and unzip to `.local/share/gnome-shell/extensions`

**or**

use GNU make:

    make install


***

## Credits

[@kaansenol5](https://github.com/kaansenol5) , [@ShrirajHegde](https://github.com/ShrirajHegde), [@AndrewZaech](https://github.com/AndrewZaech), [@vikashraghavan](https://github.com/vikashraghavan) - help with development

[@Fausto-Korpsvart](https://github.com/Fausto-Korpsvart), [Frippery Applications Menu](https://extensions.gnome.org/extension/13/applications-menu/) - Original Code

***

This project is not officially connected with Fedora, Red Hat, Ubutnu, Canonical, GNOME, Debian, OpenSUSE, System76, Solus, Raspberry Pi or any associated entity.
